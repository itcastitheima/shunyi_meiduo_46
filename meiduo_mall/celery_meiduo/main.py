from celery import Celery
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'meiduo_mall.settings')

# 一定要在 celery实例创建前 告知系统 当前的配置文件在哪里

# 1. 创建celery实例对象
# main 就是给celery实例对象设置一个名字.这个名字一般采用任务名字 或者脚本文件名字
app = Celery(main='meiduo')


# 2. 加载broker
# 参数: 指定文件路径就可以
app.config_from_object('celery_meiduo.conf')

# 3. celery 自动检测我们的任务
# 参数是一个列表
# 列表的元素是 指向任务的包路径
app.autodiscover_tasks(['celery_meiduo.sms','celery_meiduo.email'])