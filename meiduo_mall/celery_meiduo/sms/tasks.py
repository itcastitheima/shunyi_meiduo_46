
# 生产者(Producer):		生成任务（函数）
"""
1. celery的任务[函数] 必须要让celery实例的 task装饰器装饰
2. celery的任务[函数] 需要让celery检测到
"""
from celery_meiduo.main import app
from ronglian_sms_sdk import SmsSDK

@app.task
def celery_send_sms_code(mobile,sms_code,time):
    accId = '8aaf0708624670f20178ab6fe06b0'
    accToken = '3b9e8ef5848b483fe3f0eb0862f0c'
    appId = '8aaf0708624670f20162578a06b6'
    sdk = SmsSDK(accId, accToken, appId)

    # 2. 调用发送短信的方法
    tid = '1'  # 只能用模板为 1
    mobile = '{}'.format(mobile)
    datas = (sms_code, time)
    # 我们就不判断发送成功与否了
    sdk.sendMessage(tid, mobile, datas)
