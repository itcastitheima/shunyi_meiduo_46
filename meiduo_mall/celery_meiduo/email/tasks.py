from django.core.mail import send_mail
from celery_meiduo.main import app


@app.task
def celery_send_email(email,verify_url):

    # subject,      主题
    subject = '美多商城激活邮件'
    # message,      消息 内容
    message = ''  # 如果使用 html_message message为一个空字符串就可以
    # from_email,   发件人
    from_email = '美多商城官方邮箱<qi_rui_hua@163.com>'

    # recipient_list,  收件人列表
    recipient_list = [email]

    html_message = '<p>尊敬的用户您好！</p>' \
                   '<p>感谢您使用美多商城。</p>' \
                   '<p>您的邮箱为：%s 。请点击此链接激活您的邮箱：</p>' \
                   '<p><a href="%s">%s<a></p>' % (email, verify_url, verify_url)

    send_mail(subject,
              message,
              from_email,
              recipient_list,
              html_message=html_message)
