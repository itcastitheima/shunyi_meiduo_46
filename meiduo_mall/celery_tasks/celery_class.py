from celery_tasks.broker import Broker
from celery_tasks.worker import Worker

class Celery(object):
    def __init__(self):
        self.broker = Broker()		#队列
        self.worker = Worker()		#消费者

    def add(self, func):			#添加任务
        self.broker.broker_list.append(func)

    def work(self, func):			#执行任务
        self.worker.run(self.broker,func)

from celery_tasks.tasks import send_sms_code

app=Celery()
app.add(send_sms_code)

app.work(send_sms_code)

