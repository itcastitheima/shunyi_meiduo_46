from django.shortcuts import render

# Create your views here.
"""
需求: 
    做页面静态化
    
    1.将查询的动态数据
    2.渲染到模板上
    3.渲染的HTML 写入到指定文件
    4.这个指定文件 其实就是 将 8000的代码[生成的HTML] 写入到 8080的位置

"""
from utils.goods import get_categories,get_contents
from django.template.loader import get_template
def generic_detail_html():
    print('页面静态化')
    # 1.将查询的动态数据
    context = {
        'categories':get_categories(),
        'contents':get_contents()
    }
    # 2.渲染到模板上
    # 2.1 获取模板  get_template会读取模板目录下的模板文件
    template = get_template('index.html')
    # 2.2 数据渲染
    html_data = template.render(context)
    # 3.渲染的HTML 写入到指定文件
    # 4.这个指定文件 其实就是 将 8000的代码[生成的HTML] 写入到 8080的位置
    # file_path = '/home/ubuntu/Desktop/46/shunyi_meiduo_46/front_end_pc/' + 'index.html'

    from django.conf import settings
    import os
    front_path = os.path.join(os.path.dirname(settings.BASE_DIR),'front_end_pc')

    file_path =  os.path.join(front_path,'index.html')

    with open(file_path,'w') as f:
        f.write(html_data)

    print('ok')
