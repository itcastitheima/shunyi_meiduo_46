from django.urls import path
from apps.order.views import OrderSettlementView,OrderCommitView

urlpatterns = [
    path('orders/settlement/',OrderSettlementView.as_view()),

    #提交订单
    path('orders/commit/',OrderCommitView.as_view()),
]