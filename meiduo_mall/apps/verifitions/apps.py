from django.apps import AppConfig


class VerifitionsConfig(AppConfig):
    name = 'verifitions'
