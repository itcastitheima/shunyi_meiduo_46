from ronglian_sms_sdk import SmsSDK

# 1. 创建容联云 SDK实例对象
"""
accId	    String	开发者主账号,登陆云通讯网站后,可在控制台首页看到开发者主账号ACCOUNT SID
accToken	String	主账号令牌 TOKEN,登陆云通讯网站后,可在控制台首页看到主账号令牌AUTH TOKEN
appId	    String	请使用管理控制台中已创建应用的APPID
"""
accId = '8aaf0708670f20162578ab6fe06b0'
accToken = '3b9e8ef5ec5848b483f0eb0862f0c'
appId = '8aaf0708624670f20168ab75206b6'

sdk = SmsSDK(accId, accToken, appId)


# 2. 调用发送短信的方法
"""
tid	    String	短信模板 ID
mobile	String	发送手机号，多个以英文逗号分隔，最多 200 个号码
datas	tuple	替换短信模板占位符的内容变量
"""
tid = '1'               # 只能用模板为 1
mobile = '18310820688'             # '13312341234,13343214321,....'  测试手机号发送
datas = ('666888',5)    # 【云通讯】您的验证码是{1}，请于{2}分钟内正确输入。其中{1}和{2}为短信模板参数。

result = sdk.sendMessage(tid, mobile, datas)

# 3. 响应参数的判断
print(result)

import json
data = json.loads(result)
print(data)
"""
statusCode	    String	状态码，000000 为发送成功
dateCreated	    String	短信的创建时间，格式：yyyyMMddHHmmss
smsMessageSid	tuple	短信唯一标识符

"""
if data.get('statusCode') == '000000':
    print('发送成功')
else:
    print('发送失败')


