from django.urls import path
from apps.verifitions.views import ImageCodeView,SMSCodeView

urlpatterns = [
    path('image_codes/<uuid>/',ImageCodeView.as_view()),

    # 发送短信的url
    path('sms_codes/<mobile>/',SMSCodeView.as_view()),
]