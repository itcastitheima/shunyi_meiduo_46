from rest_framework.viewsets import ModelViewSet

from apps.meiduo_admin.serializers.admin import AdminModelSerializer
from apps.meiduo_admin.utils import PageNum
from apps.users.models import User


class AdminModelViewSet(ModelViewSet):

    queryset = User.objects.filter(is_staff=True)

    serializer_class = AdminModelSerializer

    pagination_class = PageNum


from rest_framework.generics import ListAPIView
from django.contrib.auth.models import Group
from apps.meiduo_admin.serializers.group import GroupModelSerializer
class GroupSimpleListAPIView(ListAPIView):

    queryset = Group.objects.all()

    serializer_class = GroupModelSerializer
