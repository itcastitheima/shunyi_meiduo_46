

"""

1. 先实现 查询所有用户
2. 再实现搜索功能
3. 实现分页
"""

# APIView               没有做任何的封装和抽取
# GenericAPIView        通常要设置 序列化器和查询结果集  一般和mixin配合使用
# ListAPIView           get方法都不用写
# ModelViewSet          增删改查

from rest_framework.generics import ListAPIView
from apps.users.models import User
from apps.meiduo_admin.serializers.user import UserModelSerializer
from rest_framework.pagination import PageNumberPagination
from apps.meiduo_admin.utils import PageNum
from rest_framework.mixins import CreateModelMixin
from rest_framework.generics import ListCreateAPIView

class UserListAPIView(ListCreateAPIView):

    # queryset = User.objects.all()

    def get_queryset(self):

        # 1. 获取搜索的查询参数
        keyword = self.request.query_params.get('keyword')
        # 2. 根据搜索的查询参数进行判断
        if keyword:
            # 如果有查询数据,则返回搜索的数据
            return User.objects.filter(username__contains=keyword)
        else:
            # 如果没有查询数据,则返回所有的用户信息
            return User.objects.all()

    serializer_class = UserModelSerializer

    #设置分页类
    pagination_class = PageNum

    # def post(self,request):
    #     return self.create(request)
    # def get(self,request):
    #
    #     keyword = request.query_params.get('keyword')
    #
    #     user = User.objects.filter(username__contains=keyword)
    #     user = User.objects.all()
    #
    #     return self.list(request)