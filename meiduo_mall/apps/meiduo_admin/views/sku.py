from rest_framework.viewsets import ModelViewSet
from apps.goods.models import SKU
from apps.meiduo_admin.serializers.sku import SKUModelSerializer
from apps.meiduo_admin.utils import PageNum

"""
caption: "11"
category_id: 115
cost_price: "11"
is_launched: "true"
market_price: "11"
name: "11"
price: "11"
spu_id: 2
stock: "111"

specs: [{spec_id: "4", option_id: 10}, {spec_id: "5", option_id: 11}]

"""
from rest_framework.permissions import DjangoModelPermissions
class SKUModelViewSet(ModelViewSet):

    queryset = SKU.objects.all()

    serializer_class = SKUModelSerializer

    pagination_class = PageNum


    permission_classes = [
        DjangoModelPermissions
    ]

"""
新增SKU数据步骤

- Step 1:获取三级分类信息
- Step 2: 获取SPU表数据
- Step 3: 获取SPU商品规格信息
- Step 4: 保存SKU数据

"""
from apps.goods.models import GoodsCategory

"""
parent_id   为 NULL   一级分类
parent_id   为 1~37   二级分类
parent_id   为 38~114 三级分类

"""

# GoodsCategory.objects.filter(parent_id__gt=37)
# GoodsCategory.objects.filter(subs=None)
from apps.meiduo_admin.serializers.sku import GoodsCategoryModelSerializer
from rest_framework.response import Response
############一级视图####################################################
from rest_framework.views import APIView
class GoodsCategoryAPIView(APIView):

    def get(self,request):

        gcs = GoodsCategory.objects.filter(subs=None)

        serializer = GoodsCategoryModelSerializer(gcs,many=True)

        return Response(serializer.data)
############二级视图#############################################################

from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin

class GoodCategoryGenericAPIView(GenericAPIView,ListModelMixin):

    queryset = GoodsCategory.objects.filter(subs=None)

    serializer_class = GoodsCategoryModelSerializer

    def get(self,request):
        return self.list(request)

###########三级视图################################################################
from rest_framework.generics import ListAPIView

class GoodCategoryListAPIView(ListAPIView):

    queryset = GoodsCategory.objects.filter(subs=None)

    serializer_class = GoodsCategoryModelSerializer



##########获取所有的SPU#######################################
from rest_framework.generics import ListAPIView
from apps.goods.models import SPU
from apps.meiduo_admin.serializers.sku import SimpleSPUModelSerializer
class SimpleSPUListAPIView(ListAPIView):

    queryset = SPU.objects.all()

    serializer_class = SimpleSPUModelSerializer

##################################


# 我们选择不同的spu的时候 肯定是知道 spu_id  . spu_id = 2
# 1. 根据spu_id 查询 规格
# specs=SPUSpecification.objects.filter(spu_id=2)

# 2. 根据规格 获取选项信息

from apps.goods.models import SPUSpecification
from apps.meiduo_admin.serializers.sku import SPUSpecificationModelSerializer
class SpecAPIView(APIView):


    def get(self,request,spu_id):

        # 1. 根据spu_id 查询 规格
        specs = SPUSpecification.objects.filter(spu_id=spu_id)

        # 2. 创建序列化器
        serializer = SPUSpecificationModelSerializer(specs,many=True)

        return Response(serializer.data)

