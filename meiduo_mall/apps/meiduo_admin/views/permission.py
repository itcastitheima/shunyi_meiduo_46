from django.contrib.auth.models import Permission       # 权限
from django.contrib.auth.models import Group            # 组
# from django.contrib.auth.models import User
from apps.meiduo_admin.serializers.permission import PermissionModelSerializer
from apps.meiduo_admin.utils import PageNum
from apps.users.models import User                      #用户 -- 管理员用户


from rest_framework.viewsets import ModelViewSet

class PermissionModelViewSet(ModelViewSet):

    queryset = Permission.objects.all()

    serializer_class = PermissionModelSerializer

    pagination_class = PageNum


from django.contrib.auth.models import ContentType
# 子应用名字
# 模型名字
# 权限类型
from rest_framework.generics import ListAPIView
from apps.meiduo_admin.serializers.permission import ContentTypeModelSerializer
class ContentTypeListAPIView(ListAPIView):

    queryset = ContentType.objects.all()

    serializer_class = ContentTypeModelSerializer