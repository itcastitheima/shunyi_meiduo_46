from rest_framework.viewsets import ModelViewSet
from django.contrib.auth.models import Group

from apps.meiduo_admin.serializers.group import GroupModelSerializer
from apps.meiduo_admin.utils import PageNum


class GroupModelViewSet(ModelViewSet):
    queryset = Group.objects.all()

    serializer_class = GroupModelSerializer

    pagination_class = PageNum


# 获取所有的权限
from django.contrib.auth.models import Permission
from rest_framework.generics import ListAPIView
from apps.meiduo_admin.serializers.permission import PermissionModelSerializer


class SimplePermissionAPIView(ListAPIView):
    queryset = Permission.objects.all()

    serializer_class = PermissionModelSerializer