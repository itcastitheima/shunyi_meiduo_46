from django.urls import path

# from rest_framework_jwt.views import obtain_jwt_token
from apps.meiduo_admin.login import meiduo_admin_login
from apps.meiduo_admin.views import home,user,image,sku,permission,group,admin
urlpatterns = [
    path('authorizations/',meiduo_admin_login),


    # 统计日活
    path('statistical/day_active/',home.DailyActiveCountAPIView.as_view()),

    # 日下单统计
    path('statistical/day_orders/',home.DailyOrderCountAPIView.as_view()),

    # 月增用户统计
    path('statistical/month_increment/',home.MonthAddCountAPIView.as_view()),

    #用户管理
    path('users/',user.UserListAPIView.as_view()),

    # 新增图片中 获取所有的sku
    path('skus/simple/',image.SKUSimpleListAPIView.as_view()),

    # 新增SKU中 获取三级分类数据
    path('skus/categories/',sku.GoodsCategoryAPIView.as_view()),
    # 新增SKU中 获取spu数据
    path('goods/simple/',sku.SimpleSPUListAPIView.as_view()),


    # 根据spu_id 获取规格和选项
    path('goods/<spu_id>/specs/',sku.SpecAPIView.as_view()),


    # 权限 -- 获取所有的 content-type
    path('permission/content_types/',permission.ContentTypeListAPIView.as_view()),

    # 组 -- 获取所有的 权限
    path('permission/simple/',group.SimplePermissionAPIView.as_view()),
    # 管理员 -- 获取所有组
    path('permission/groups/simple/',admin.GroupSimpleListAPIView.as_view()),
]

from rest_framework.routers import DefaultRouter,SimpleRouter
# 1.创建router实例对象
router =DefaultRouter()
# 2. 注册url
router.register('skus/images',image.SKUImageModelViewSet,basename='images')
# 3. 将router生成的url追加到urlpatterns
urlpatterns += router.urls



# 2. 注册url
router.register('skus',sku.SKUModelViewSet,basename='skus')
# 3. 将router生成的url追加到urlpatterns
urlpatterns += router.urls


##############权限
# 2. 注册url
router.register('permission/perms',permission.PermissionModelViewSet,basename='perms')
# 3. 将router生成的url追加到urlpatterns
urlpatterns += router.urls


##############组
# 2. 注册url
router.register('permission/groups',group.GroupModelViewSet,basename='groups')
# 3. 将router生成的url追加到urlpatterns
urlpatterns += router.urls

##############管理员
# 2. 注册url
router.register('permission/admins',admin.AdminModelViewSet,basename='admins')
# 3. 将router生成的url追加到urlpatterns
urlpatterns += router.urls