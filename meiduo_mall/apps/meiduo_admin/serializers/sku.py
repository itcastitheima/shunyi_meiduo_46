from rest_framework import serializers
from apps.goods.models import SKU,SKUSpecification

# 定义 规格和选项的序列化器
class SKUSpecificationModelSerializer(serializers.ModelSerializer):

    spec_id=serializers.IntegerField()
    option_id=serializers.IntegerField()

    class Meta:
        model = SKUSpecification
        fields = ['spec_id','option_id']

class SKUModelSerializer(serializers.ModelSerializer):
    # 就把分类的字段类型修改了
    category=serializers.StringRelatedField()
    # 前端还需要 category_id 的分类id数据
    category_id=serializers.IntegerField()


    spu = serializers.StringRelatedField()
    spu_id = serializers.IntegerField()

    # sku的级联字段 -- 规格和选项
    specs = SKUSpecificationModelSerializer(many=True)

    class Meta:
        model = SKU
        fields = '__all__'


    def create(self, validated_data):
        # 1. 把 多的数据 拿出来 -- 规格和选项
        specs = validated_data.pop('specs')
        # with 语句 会自动的提交 或者 出现异常 回滚
        from django.db import transaction
        with transaction.atomic():
            # 2. 剩下的就是 1的数据 -- SKU数据 单独保存
            sku = SKU.objects.create(**validated_data)
            # 3. 对多的数据进行遍历
            for spec in specs:
                # raise Exception('abc')
                # spec = {spec_id: "4", option_id: 10}
                SKUSpecification.objects.create(sku=sku,**spec)

        return sku


    def update(self, instance, validated_data):

        # 1. 把数据分离 . 把嵌套的字典数据分开.分为 1的数据和多的数据
        specs = validated_data.pop('specs')
        # 2. 更新1的数据
        # instance -- sku
        # sku.name = validated_data.get('name',instance.name)

        super().update(instance,validated_data)

        # for attr, value in validated_data.items():
        #     setattr(instance, attr, value)
        # instance.save()

        # 3. 更新多的数据
        for spec in specs:
            # spec = {spec_id: "4", option_id: 8}
            SKUSpecification.objects.filter(sku=instance,spec_id=spec.get('spec_id')).update(option_id=spec.get('option_id'))

        return instance

#三级分类数据的获取#########################
from apps.goods.models import GoodsCategory

class GoodsCategoryModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = GoodsCategory
        fields = '__all__'


#####spu的序列化器############################
from apps.goods.models import SPU

class SimpleSPUModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = SPU
        fields = ['id','name']


######规格和选项的序列化器####################################

from apps.goods.models import SPUSpecification,SpecificationOption
#选项
class SpecificationOptionModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = SpecificationOption
        fields = ['id','value']
# 规格
class SPUSpecificationModelSerializer(serializers.ModelSerializer):
    # 选项的序列化器
    options = SpecificationOptionModelSerializer(many=True)

    class Meta:
        model = SPUSpecification
        fields = ['id','name','options']
