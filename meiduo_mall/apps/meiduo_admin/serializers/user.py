
from rest_framework import serializers
from apps.users.models import User
class UserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id','mobile','email','username','password']

        extra_kwargs = {
            'password':{
                'write_only': True,
                'min_length':5
            }
        }



    def create(self, validated_data):

        # user = User.objects.create(**validated_data)
        # user.set_password(validated_data.get('password'))
        # user.save()

        return User.objects.create_user(**validated_data)