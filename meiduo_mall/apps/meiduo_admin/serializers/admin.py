from rest_framework import serializers
from apps.users.models import User

class AdminModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'
        # fields = ['id','username','mobile','email']

    def create(self, validated_data):
        # 先调用父类的方法,实现用户信息的保存
        user = super().create(validated_data)

        # user = User.objects.create_user(**validated_data)
        #user需要设置为 普通管理员


        # 再单独设置 相关的问题字段
        user.is_staff=1
        user.set_password(validated_data.get('password'))
        user.save()
        return user