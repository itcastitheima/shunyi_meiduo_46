def jwt_response_payload_handler(token, user=None, request=None):
    # token  认证成功之后,系统生成的token
    # user  认证成功之后 用户的对象
    # request  登录的请求
    return {
        'token': token,
        'username': user.username,
        'id':user.id
    }


from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from collections import OrderedDict
class PageNum(PageNumberPagination):

    # 1. 开启分页
    # 2. 如果没有传递 1页多少条数据,默认返回 5条
    page_size = 5

    # 1. 开启 每页多少条数据的参数
    # 2. vue的axios写死了参数 我们必须写成 pagesize
    page_size_query_param = 'pagesize'

    def get_paginated_response(self, data):

        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('lists', data),
            ('page', self.page.number),                         # 当前的页码
            ('pages', self.page.paginator.num_pages),            # 分了多少页
            ('pagesize', self.page_size),                       # 一页多少条数据
        ]))


def upload_image_to_qiniu(file_data):
    from qiniu import Auth, put_data
    # 需要填写你的 Access Key 和 Secret Key
    access_key = 'q9crPZPROOXrykaH85q_zpEEll0f_LsjXwUnXHRo'
    secret_key = 'lG_4_tI8bJTR8Zk6z8fGwYp79aQHkJgolvvBL_qm'
    # 构建鉴权对象  -- 验证我们的 AK和SK 对不对
    q = Auth(access_key, secret_key)
    # 要上传的空间
    bucket_name = 'shunyi46'
    # 上传后保存的文件名
    # key 为 None 七牛云会自动生成图片的名字 这样可以防止重复
    key = None
    # 生成上传 Token，可以指定过期时间等
    token = q.upload_token(bucket_name, key, 3600)
    # 要上传二进制流
    ret, info = put_data(token, key, file_data)
    # 4. 获取上传图片的 图片名
    return ret['key']

