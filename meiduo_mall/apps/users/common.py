from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from django.conf import settings

def generic_email_token(email,user_id):

    # 1. 创建实例对象
    s = Serializer(secret_key=settings.SECRET_KEY,expires_in=3600)
    # 2. 组织数据
    data = {
        'email':email,
        'user_id':user_id
    }
    # 3. 加密数据
    secret_data = s.dumps(data)
    # 4. 返回加密数据
    return secret_data.decode()
from itsdangerous import BadSignature,BadTimeSignature
def check_email_token(token):

    # 1.创建实例对象
    s = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)

    # 2.解密
    try:
        data = s.loads(token)
    except BadSignature:
        return None
    else:
        # 3. 返回解密数据
        return data
