
from django.urls import path
from apps.users.views import UsernameCountView,RegisterView,LoginView
from apps.users.views import LogoutView,UserCenterView
from apps.users.views import EmailView,EmailVerificationView
from apps.users.views import CreateAddressView,AddressListView
from apps.users.views import UserHistoryView
urlpatterns = [
    # 判断用户名是否重复
    # 通过 <变量名> 来获取 请求参数
    path('usernames/<usercount:username>/count/',UsernameCountView.as_view()),

    # 注册的
    path('register/',RegisterView.as_view()),

    # 登录
    path('login/',LoginView.as_view()),

    # ctl + D 就是复制一行
    path('logout/',LogoutView.as_view()),

    # 个人中心
    path('info/',UserCenterView.as_view()),

    #更新[保存]邮箱信息
    path('emails/',EmailView.as_view()),

    #邮件激活
    path('emails/verification/',EmailVerificationView.as_view()),

    #新增收货地址
    path('addresses/create/',CreateAddressView.as_view()),

    #获取收货地址
    path('addresses/',AddressListView.as_view()),
    #新增用户浏览记录
    path('browse_histories/',UserHistoryView.as_view()),
]