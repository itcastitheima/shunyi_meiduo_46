from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from django.conf import settings
#加密
def generic_open_id(openid):

    # 1.创建实例对象
    s = Serializer(secret_key=settings.SECRET_KEY,expires_in=3600)
    # 2.组织数据
    data = {
        'openid':openid
    }
    # 3.加密
    secret_data = s.dumps(data)

    # bytes --> str
    return secret_data.decode()

#解密
from itsdangerous import BadSignature
def check_open_id(serect_data):
    # 1.创建实例对象
    s = Serializer(secret_key=settings.SECRET_KEY, expires_in=3600)

    # 2.解密
    try:
        data = s.loads(serect_data)
    except BadSignature:
        return None
    else:
        return data.get('openid')
