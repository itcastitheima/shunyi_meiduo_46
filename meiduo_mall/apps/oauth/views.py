from django.shortcuts import render

# Create your views here.
"""

需求:     获取用户同意QQ登录[QQ用户扫描]的code

前端:
        前端需要获取code.然后通过axios请求 将code传递给后端服务器
    
后端:
    请求
            GET         oauth_callback/?code=xxx
    业务
            1.获取参数
            2.通过code换取token
            3. 通过token换取openid
    响应
            暂时不返回
"""

from django.views import View
from django.http import JsonResponse
from QQLoginTool.QQtool import OAuthQQ
class QQLoginView(View):

    def get(self,request):

        # 1.获取参数
        code = request.GET.get('code')
        if code is None:
            return JsonResponse({'code':'400','errmsg':'没有code信息'})

        # client_id=None,               appid
        # client_secret=None,           appkey
        # redirect_uri=None,            当用户扫描同意之后,应该跳转到美多商城的页面
        # state=None                    不知道什么意思 就随便写.等出错了 再回来分析
        """
        # QQ登录参数
        # 我们申请的 客户端id
        QQ_CLIENT_ID = '101474184'
        # 我们申请的 客户端秘钥
        QQ_CLIENT_SECRET = 'c6ce949e04e12ecc909ae6a8b09b637c'
        # 我们申请时添加的: 登录成功后回调的路径
        QQ_REDIRECT_URI = 'http://www.meiduo.site:8080/oauth_callback.html'
        """
        # QQ登录参数
        # 我们申请的 客户端id
        QQ_CLIENT_ID = '101474184'
        # 我们申请的 客户端秘钥
        QQ_CLIENT_SECRET = 'c6ce949e04e12ecc909ae6a8b09b637c'
        # 我们申请时添加的: 登录成功后回调的路径
        QQ_REDIRECT_URI = 'http://www.meiduo.site:8080/oauth_callback.html'
        qq = OAuthQQ(client_id=QQ_CLIENT_ID,
                     client_secret=QQ_CLIENT_SECRET,
                     redirect_uri=QQ_REDIRECT_URI,
                     state='lalala')
        # 2.通过code换取token
        token = qq.get_access_token(code)
        # 3. 通过token换取openid
        # 'CBCF1AA40E417CD73880666C3D6FA1D6'
        openid = qq.get_open_id(token)


        # 4. 根据openid进行数据的查询
        from apps.oauth.models import OAuthQQUser
        try:
            qquser = OAuthQQUser.objects.get(openid=openid)
        except OAuthQQUser.DoesNotExist:
            # 如果查询不出用户信息,说明 没有绑定过 ,我们要把openid,以响应的形式 给前端页面
            from apps.oauth.common import generic_open_id
            secret_openid = generic_open_id(openid)

            return JsonResponse({'code':200,'access_token':secret_openid})
        else:

            # 如果查询出 用户信息,说明 用户之前绑定过.设置状态保持,跳转到指定页面
            from django.contrib.auth import login
            # user 必须是 User 的实例对象
            login(request,user=qquser.user)

            # 0 表示已经绑定过了
            #非０表示未绑定
            response = JsonResponse({'code': 0, 'errmsg': 'ok'})
            response.set_cookie('username', qquser.user.username, max_age=24 * 3600)
            return response

    def post(self,request):
        # 1.接收请求参数
        import json
        data = json.loads(request.body.decode())
        # 2. 获取请求参数
        moible=data.get('mobile')
        password=data.get('password')
        sms_code=data.get('sms_code')
        secret_data = data.get('access_token')

        # 这里解密数据
        from apps.oauth.common import check_open_id
        openid = check_open_id(secret_data)
        if openid is None:
            return JsonResponse({'code':400,'errmsg':'参数错误'})
        # 3. 验证参数 -- 验证手机号,验证密码,验证短信验证码

        # 验证短信验证码
        # 3.1 连接redis
        from django_redis import get_redis_connection
        redis_cli = get_redis_connection('code')
        # 3.2 获取redis的短信验证码
        redis_sms_code = redis_cli.get(moible)
        if redis_sms_code is None:
            return JsonResponse({'code':400,'errmsg':'短信验证码过期'})
        # 3.3 比对
        if redis_sms_code.decode() != sms_code:
            return JsonResponse({'code': 400, 'errmsg': '短信验证码错误'})

        # 4. 根据用户的手机号来查询判断 该手机号是否已经注册了用户
        from apps.users.models import User
        try:
            user = User.objects.get(mobile=moible)
        except User.DoesNotExist:
            #     如果没有注册则需要新建用户
            user = User.objects.create_user(
                username=moible,        #让手机号作为用户名
                mobile=moible,
                password=password
            )
        else:
            #     如果注册了需要验证密码
            # user.check_password(password) 返回一个布尔值
            # 密码没问题返回True
            # 密码有问题返回False
            if not user.check_password(password):
                return JsonResponse({'code': 400, 'errmsg': '手机号或密码错误'})



        # 5. 绑定openid和用户信息[数据入库]
        from apps.oauth.models import OAuthQQUser
        OAuthQQUser.objects.create(
            user=user,
            openid=openid
        )
        # 6. 设置状态保持
        from django.contrib.auth import login
        login(request,user)
        # 7. 返回响应
        response = JsonResponse({'code':0,'errmsg':'ok'})
        response.set_cookie('username',user.username,max_age=24*3600)
        return response
"""
需求：
        让ＱＱ登录用户的openid和 用户信息绑定

前端：
        当用户输入完 手机号,密码,图片验证码,短信验证码之后,会点击绑定按钮
        前端要收集  openid,手机号,密码和短信验证码.发送给后端

后端：

    请求                  POST            body  openid,手机号,密码和短信验证码.发送给后端 JSOn
    
    业务逻辑
                    1.接收请求参数
                    2. 获取请求参数
                    3. 验证参数 -- 验证手机号,验证密码,验证短信验证码
                    4. 根据用户的手机号来查询判断 该手机号是否已经注册了用户
                        如果注册了需要验证密码
                        如果没有注册则需要新建用户
                    5. 绑定openid和用户信息[数据入库]
                    6. 设置状态保持
                    7. 返回响应
    响应
                    JSON        code:0
"""

###########itsdangerous   ---加密 ###############################################

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
# 加密操作 3步
# 1. 创建 实例对象
# secret_key,           秘钥
# expires_in=None       过期时间是 秒数
s = Serializer(secret_key='abc', expires_in=3600)
# 2. 组织数据
data = {
    'openid':'1234567890'
}
# 3. 加密
s.dumps(data)

##############itsdangerous   ---解密 ################
from itsdangerous import BadSignature
# 解密 2步
# 1. 创建 实例对象
try:
    s = Serializer(secret_key='abc', expires_in=3600)
    # 2. 解密数据
    s.loads('')
except BadSignature:
    print('数据被修改')
