from django.urls import path
from apps.oauth.views import QQLoginView
urlpatterns = [
    path('oauth_callback/',QQLoginView.as_view()),
]