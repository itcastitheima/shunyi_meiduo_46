from django.urls import path
from apps.payment.views import AlipayURLView,PaymentStatusView

urlpatterns = [
    # 保存支付宝订单id
    path('payment/status/', PaymentStatusView.as_view()),

    path('payment/<order_id>/',AlipayURLView.as_view()),


]