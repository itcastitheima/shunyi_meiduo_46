from django.urls import path
from apps.areas.views import ProvinceView,SubAreaView

urlpatterns = [
    #省数据
    path('areas/',ProvinceView.as_view()),

    #市 区县
    path('areas/<parent_id>/',SubAreaView.as_view())
]