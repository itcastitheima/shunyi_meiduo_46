from django.shortcuts import render

# Create your views here.

# 省
"""
需求:    
        获取省份信息
前端:
        发送一个ajax[axios]请求
后端:

    请求:
                GET     areas/          JSON
    业务逻辑:
            1. 查询数据库
            2. 将查询结果集遍历 转换为字典字典数据
            3. 返回响应
    响应
        "code":"0",
          "errmsg":"OK",
          "province_list":[
              {
                  "id":110000,
                  "name":"北京市"
              },
              ......
          ]

"""
from django.views import View
from apps.areas.models import Area
from django.http import JsonResponse
from django.core.cache import cache
class ProvinceView(View):
    def get(self,request):

        # 先读取缓存
        data_list = cache.get('province')

        # 判断缓存是否有数据
        if data_list is None:
            #如果没有缓存数据,则重新查询
            # 1. 查询数据库
            areas = Area.objects.filter(parent__isnull=True)

            data_list = []
            # 2. 将查询结果集遍历 转换为字典字典数据
            for area in areas:
                data_list.append({
                    'id':area.id,
                    'name':area.name
                })

            # 这里缓存
            # cache.set(key,value,seconds)
            # 过期时长 要结合具体的需求
            cache.set('province',data_list,24*3600)

        # 3. 返回响应
        return JsonResponse({"code":"0",
                          "errmsg":"OK",
                          "province_list":data_list})



# 市 区县
"""
需求:
        获取市 区县数据
前端:
        当用户选择 省[市] 的时候,要把 省[市] 的id 获取到,以请求的形式传递给后台服务器
                
后端:
        
        请求:
                        GET     areas/parent_id/
        业务逻辑:
                    1.获取 省[市] 的id
                    2. 根据 省[市] 的id 获取下一级数据
                    3. 将查询结果集遍历转换为字典列表
                    4. 返回响应
        响应
                "code":"0",
                  "errmsg":"OK",
                  "sub_data":{
                      "subs":[
                          {
                              "id":130100,
                              "name":"石家庄市"
                          },
                          ......
                      ]
                  }
"""

class SubAreaView(View):

    def get(self,request,parent_id):

        subs_list = cache.get('city_%s'%parent_id)

        if subs_list is None:

            # 1.获取 省[市] 的id
            # 2. 根据 省[市] 的id 获取下一级数据
            #方式1
            # areas = Area.objects.filter(parent_id=parent_id)
            # 方式2
            # ① 根据 parent_id 获取 省 [市]
            pro_city_id = Area.objects.get(id=parent_id)
            # areas = pro_city_id.area_set.all()
            areas = pro_city_id.subs.all()

            subs_list = []
            # 3. 将查询结果集遍历转换为字典列表
            for area in areas:
                subs_list.append({
                    'id':area.id,
                    'name':area.name
                })

            cache.set('city_%s'%parent_id,subs_list,24*3600)
        # 4. 返回响应
        return JsonResponse({
            "code": "0",
            "errmsg": "OK",
            "sub_data": {
                "subs": subs_list
            }
        })
