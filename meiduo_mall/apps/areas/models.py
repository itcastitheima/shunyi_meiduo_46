from django.db import models

class Area(models.Model):
    """省"""
    # id
    name = models.CharField(max_length=20, verbose_name='名称')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL,
                               related_name='subs',
                               null=True, blank=True, verbose_name='上级行政区划')

    # 因为省市区的模型是自关联,所以 它内部的 关联属性 能够获取下一级数据
    # area_set = [Area,Area,Area,Area,...]

    # 默认的关联模型属性是  关联模型类名小写 我们可以通过 related_name改变它的默认值
    # subs = [Area,Area,Area,Area,...]

    # subs 和 area_set 就是名字不一样 效果一样

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '省市区'
        verbose_name_plural = '省市区'

    def __str__(self):
        return self.name


"""
省 / 市 / 区县

id          name            parent_id

10000       河北省             NULL



11000       石家庄市            10000
12000       保定市              10000



11001       高新区              11000
11002       长安区              11000



select * from tb_areas where parent_id is NULL;  省

select * from tb_areas where parent_id=130000;    市
select * from tb_areas where parent_id=130100;    区县


"""

