from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from django.views import View
from utils.goods import get_categories,get_contents
class IndexView(View):

    def get(self,request):

        context = {
            'categories':get_categories(),
            'contents':get_contents()
        }

        return render(request,'index.html',context=context)


"""
需求:
        实现列表页面的数据获取
前端:
        list/115/skus/?page=1&page_size=5&ordering=-create_time

后端:
    
        请求:
                GET     
        业务逻辑:
                1. 接收参数
                2. 提起参数
                3. 验证参数
                4. 查询数据 
                    4.1 根据分类id查询数据
                    4.2 查询的数据排序
                    4.3 查询的数据分页
                5. 返回响应
        返回响应:
            "code": 0,
            "errmsg": "ok",
            "list": [
                {
                    "id": 16,
                    "default_image_url": "http://qmkds4f73.hnbkt.clouddn.com/CtM3BVrRdPeAXNDMAAYJrpessGQ9777651.png",
                    "name": "华为 HUAWEI P10 Plus 6GB+128GB 曜石黑 移动联通电信4G手机 双卡双待",
                    "price": "3788.00"
                },
                ...
            ],
            "count": 3
"""
from apps.goods.models import SKU
class ListSKUView(View):

    def get(self,request,category_id):
        # 1. 接收参数
        query_params = request.GET
        # 2. 提起参数
        ordering = query_params.get('ordering','-createtime')       # 排序
        page_size = query_params.get('page_size',5)                 # 一页多少条记录
        page = query_params.get('page',1)                           # 第几页
        # 3. 验证参数
        # 4. 查询数据
        #     4.1 根据分类id查询数据
        #     4.2 查询的数据排序
        skus = SKU.objects.filter(category=category_id).order_by(ordering)
        #     4.3 查询的数据分页
        from django.core.paginator import Paginator
        # ① 创建分页类
        # object_list 查询结果集/列表
        # per_page 每页多少条记录
        instance = Paginator(skus,page_size)
        # ② 获取指定页数的数据
        page_skus = instance.page(page)
        # ③ 获取总页数
        total = instance.num_pages


        # 将对象列表转换为字典
        data_list = []
        for item in page_skus.object_list:
            data_list.append({
                'id':item.id,
                'name':item.name,
                'price':item.price,
                'default_image_url':item.default_image.url      #注意 调用url 因为要把图片 拼接 七牛云的外链
            })


        #面包屑数据
        # 获取三级分类
        from apps.goods.models import GoodsCategory
        from utils.goods import get_breadcrumb
        category = GoodsCategory.objects.get(id=category_id)
        breadcrumb = get_breadcrumb(category)

        # 5. 返回响应
        return JsonResponse({
            "code": 0,
            "errmsg": "ok",
            "breadcrumb":breadcrumb,
            "list": data_list,
            "count": total
        })


class HotSKUView(View):

    def get(self,request,category_id):

        # skus = SKU.objects.filter(category_id=category_id)
        skus = SKU.objects.filter(category=category_id).order_by('-sales')[:2]

        # 将对象列表转换为字典
        data_list = []
        for item in skus:
            data_list.append({
                'id': item.id,
                'name': item.name,
                'price': item.price,
                'default_image_url': item.default_image.url  # 注意 调用url 因为要把图片 拼接 七牛云的外链
            })


        return JsonResponse({
                "code":"0",
                "errmsg":"OK",
                "hot_skus": data_list})


"""

表和表直接的关系:  1 对 1  , 1 对 多 , 多  对  多

1 对 1   一般是 2个表
 

 
1 对 多 , 一般是 2个表



多  对  多, 一般是3个表


学生 和 老师

学生id        学生name          
  1             张三          
  2             李四
  
老师id        老师name
    666         田老师
    999         张老师

第三张 关系表
stu_id      tea_id
  1             666
  1             999
  2             666
  2             999


"""

class DetailView(View):

    def get(self,request,sku_id):
        """
        我们的详情页面  和 首页是一样的 都是 先经过模板渲染 最后将页面静态化

        1. 根据sku_id查询sku数据
        2. 获取分类数据
        3. 获取面包屑数据
        4. 获取规格和规格选项
        """
        # 1. 根据sku_id查询sku数据
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return JsonResponse({'code':'400','errmsg':'没有此商品'})
        # 2. 获取分类数据
        from utils.goods import get_categories
        categories = get_categories()
        # 3. 获取面包屑数据
        from utils.goods import get_breadcrumb
        breadcrumb = get_breadcrumb(sku.category)
        # 4. 获取规格和规格选项
        from utils.goods import get_goods_specs
        specs = get_goods_specs(sku)

        context = {
            'categories':categories,
            'breadcrumb':breadcrumb,
            'sku':sku,
            'specs': specs,
        }

        return render(request,'detail.html',context=context)


"""
需求:
    统计一天内该类别的商品被访问的次数。
    
    
前端:
        发送请求,传递 分类id
        
后端:
    
    请求:
            POST        /detail/visit/分类id/
    
    业务逻辑:
        
            1. 根据分类id查询分类数据
            2. 根据统计模型 查询当前的 分类记录 是否存在
            3. 如果不存在 则新增
            4. 如果存在,则访问量+1
            5. 返回响应
    响应:

            code: 0
"""
from apps.goods.models import GoodsCategory,GoodsVisitCount
from datetime import date
class CategoryVisitView(View):

    def post(self,request,category_id):
        # 1. 根据分类id查询分类数据
        try:
            category=GoodsCategory.objects.get(id=category_id)
        except GoodsCategory.DoesNotExist:
            return JsonResponse({'code':400,'errmsg':'分类信息错误'})
        # 2. 根据统计模型 查询当前的 分类记录 是否存在

        today = date.today()

        try:
            gvc = GoodsVisitCount.objects.get(category=category,
                                          date=today)
        except GoodsVisitCount.DoesNotExist:
            # 3. 如果不存在 则新增
            GoodsVisitCount.objects.create(
                category=category,
                count=1,
                date=today
            )
        else:
            # 4. 如果存在,则访问量+1
            gvc.count += 1
            gvc.save()


        # 5. 返回响应
        return JsonResponse({'code':0,'errmsg':'ok'})



from haystack.views import SearchView

class MeiduoSearchView(SearchView):


    def create_response(self):
        #调用系统的方法 获取数据
        context = self.get_context()

        #获取分页数据
        search_list = context.get('page').object_list
        #初始化列表
        data_list = []

        #遍历分页数据
        for item in search_list:
            data_list.append({
                'id':item.object.id,
                'name':item.object.name,
                'price':item.object.price,
                'default_image_url':item.object.default_image.url,
                "searchkey": context.get('query'),
                "page_size": context.get('paginator').per_page,         #一页多少条数据
                "count": context.get('paginator').count              #搜索结果有多少
            })


        return JsonResponse(data_list,safe=False)