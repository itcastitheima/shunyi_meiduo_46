from django.urls import path
from apps.goods.views import IndexView,ListSKUView,HotSKUView
from apps.goods.views import DetailView,CategoryVisitView
from apps.goods.views import MeiduoSearchView
urlpatterns = [
    # 首页
    path('index/',IndexView.as_view()),

    #列表页面
    path('list/<category_id>/skus/',ListSKUView.as_view()),

    #热销数据
    path('hot/<category_id>/',HotSKUView.as_view()),
    #详情页面模板渲染
    path('detail/<sku_id>/',DetailView.as_view()),

    #商品访问量
    path('detail/visit/<category_id>/',CategoryVisitView.as_view()),

    #搜索的url
    path('search/',MeiduoSearchView()),
]