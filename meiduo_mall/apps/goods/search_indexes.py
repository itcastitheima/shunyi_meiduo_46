import datetime
from haystack import indexes
from apps.goods.models import SKU,SPU

# 1. 类 必须继承自 indexes.SearchIndex, indexes.Indexable
# 2. 类名和模型类名一致 XxxxIndex
class SKUIndex(indexes.SearchIndex, indexes.Indexable):

    # 3. text  惯例是命名该字段text
    # 文档要求 必须有一个字段 是 document=True  为什么呢?? 进行搜索的主要字段

    # text 是进行搜索的主要字段 那这个字段的数据来源于哪呢???
    # use_template = True
    # 我们通过模板来指定哪些字段 用于 检索[搜索]
    text = indexes.CharField(document=True, use_template=True)
    # 我们不指定获取模型对应的某些字段.我们获取所有字段.
    # 获取模型对应的所有字段就不设置字段了

    # 4. 方法 -- 指定模型
    def get_model(self):
        return SKU

    # 5. 让模型查询数据,将查询的数据给haystack
    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        #      self.get_model() = SKU
        #      SKU.objects.filter()
        return self.get_model().objects.filter(is_launched=True)
        # return SKU.objects.filter(is_launched=True)          等价的
        # return self.get_model().objects.all()                 等价的 获取所有数据


