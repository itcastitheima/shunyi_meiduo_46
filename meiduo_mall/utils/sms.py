from ronglian_sms_sdk import SmsSDK

def send_sms_code(mobile, sms_code, time):
    accId = '8aaf0708670f20162578ab6fe06b0'
    accToken = '3b9e8ef5ec5848b483f0eb0862f0c'
    appId = '8aaf0708624670f20168ab75206b6'
    sdk = SmsSDK(accId, accToken, appId)

    # 2. 调用发送短信的方法
    tid = '1'  # 只能用模板为 1
    mobile = '{}'.format(mobile)
    datas = (sms_code, time)
    # 我们就不判断发送成功与否了
    sdk.sendMessage(tid, mobile, datas)