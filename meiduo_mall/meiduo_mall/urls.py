"""meiduo_mall URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

#####日志相关代码#############################
from django.http import HttpResponse
# 1. 导入日志模块
import logging

# 2. 获取日志器
logger = logging.getLogger('django')

# 3. 记录日志 -- 类视图[视图函数]
def log(request):

    logger.info('执行了登录方法')

    # BookInfo.objects.get(id=1)
    logger.error('查询不到该对象')

    logger.warning('系统的方法 要被移除了')

    return HttpResponse('log')

##################################

######注册 转换器###############
from django.urls import register_converter
from utils.converters import UsernameConverter

register_converter(UsernameConverter,'usercount')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('log/',log),

    path('',include('apps.users.urls')),
    path('',include('apps.verifitions.urls')),
    path('',include('apps.oauth.urls')),
    path('',include('apps.areas.urls')),
    path('',include('apps.goods.urls')),
    path('',include('apps.carts.urls')),
    path('',include('apps.order.urls')),
    path('',include('apps.payment.urls')),

    # 项目2的 url 都是以 meiduo_amdin 开头
    path('meiduo_admin/',include('apps.meiduo_admin.urls')),
]
